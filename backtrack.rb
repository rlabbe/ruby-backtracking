# Performs the backtracking algorithm with a validator block that you supply.
#
# Use:
#
# This class uses the standard backtracking trick of storing the current 
# search tree and solution in a single array. Each array element represents
# a level in the tree. Since we never revisit nodes in the tree, we
# can store the current state of the search in the array itself, which
# is identical to the current proposed solution. See any standard text,
# or my article Solving Combinatorial Problems with STL and backtracking:
# http://www.drdobbs.com/cpp/solving-combinatorial-problems-with-stl/184401194
#
# Define a block that accepts an array. It should return true iff the
# input parameters define a possible solution to the problem. Pass it, along
# with the size of the solution, and left/right child values, to the initialize
# function. Successively call solve() while it returns true to get successive
# solutions.
#
# Requirements:
#   * container must support array access via []
#   * container elements must support == and .succ
#   * left_child..right_child must be partially ordered such that
#     left_child.succ.succ... eventually == right_child
#
# Example:
# 
# Find all arrays of length four whose elements sum to 6 and 
# with integers in the range 0..5
# b = Backtrack.new(4, 0, 5) {|arr| 
#   sum = arr.inject(0,:+)
#   if arr.size == 4
#     sum == 6
#   else
#     sum <= 6
#   end
# }
#      
# a = []
# while b.solve(a)
#  print a, "\n"
# end
#
class Backtrack
 
  def initialize (size, left_child, right_child, &is_valid)
    @is_valid = is_valid
    @size = size
    @left_child = left_child
    @right_child = right_child
  end
   
  # Call with an empty array to generate the first solution.
  # Returns true iff a solution was found.
  # Call repeatedly with the same array to generate successive solutions.
  def solve!(arr)
    
    # if this is the first time solve is called, create a left leaf
    # otherwise assume arr contains a valid solution, so visit a new node
    # to continue the search
    if arr.size == 0
      create_left_leaf(arr)
    elsif visit_new_node(arr) == false
      return false
    end
    
   while true do
     if find_valid_sibling(arr)
       if arr.size() < @size
         create_left_leaf(arr)
       else
         return true
       end
     elsif visit_new_node(arr) == false
       # the tree has been exhausted, so no solution exists
       return false
     end
   end 
  end
  
private
  # Finds the next valid sibling of the leaf [-1]
  # Returns true iff a valid sibling was found
  def find_valid_sibling(arr)
    while true do
      return true if @is_valid.call(arr)
      return false if arr[-1] == @right_child
      arr[-1] = arr[-1].succ
    end
  end


  # Back up as long as the node is the rightmost child of its parent.
  # You can replace this if you need to implement backmarking
  def backtrack(arr)
    while arr[-1] == @right_child && arr.size > 0
      arr.pop()
    end
    
  end  
  # Backtracks through the decision tree until if finds a node
  # that hasn't been visited. Returns true iff an unvisited node
  # was found.
  def visit_new_node(arr)
    # ALGORITHM:
    #
    # If the current node is the rightmost child we must backtrack 
    # one level because there are no more children at this level.
    # So we back up until we find a non-rightmost child, then
    # generate the child to the right. If we back up to the top 
    # without finding an unvisted child, then all nodes have been 
    # generated.


    # back up to a node that we can continue expanding
    backtrack(arr)
    
    if arr.size > 0 && arr[-1] != @right_child
      arr[-1] = arr[-1].succ
      true
    else
      false
    end
  end
  
  # Add a left most leaf to the bottom of the decision tree
  def create_left_leaf(arr)
    arr << @left_child
  end
end

class Backmark < Backtrack
  def set_backtrack_level(level)
    @level = level
  end

  def backtrack(arr)
    while (arr.size() > @level)
      arr.pop()
    end    
  end
end
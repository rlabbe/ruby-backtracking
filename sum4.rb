require_relative 'backtrack'

backtrack = Backtrack.new(4, 0, 10) {|arr| 
  sum = arr.reduce(0,:+)
  if arr.size == 4
    sum == 6
  else
    sum <= 6
  end
}
      

a = []
while backtrack.solve!(a)
  print a, "\n"
end


def sumto4(arr)
  sum = arr.reduce(0,:+)
  if arr.size == 4
    sum == 4
  else
    sum <= 4
  end
end

a = []
backtrack = Backtrack.new(5, 0, 4) {|arr| sumto4(arr)}
while backtrack.solve!(a)
  print a, "\n"
end


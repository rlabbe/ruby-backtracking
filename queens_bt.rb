require_relative 'backtrack'
require 'test/unit'

# this provides several ways to compute the n-queens problem


# board representation
# store positions in an array. Each element stores the row position
# for the associated column. arr[2] = 4 puts a queen on row 4, column 2

def is_valid_board(arr)
  n = arr.size
  return true if n < 2
  myrow = arr[-1]
  
  # backtrack ensures that the n-1 positions are all valid, so just
  # validate the last element against the assumed valid n-1 elements
  range = (0...n-1)
  
  # is anyone else on this row?
  for i in range
    return false if arr[i] == myrow
  end
  
  # is anyone diagonally left?
  left_diag = myrow - n + 1 # the column diagonal to me on the zeroith row
  for i in range
    return false if arr[i] == left_diag + i
  end
  
  # is anyone diagonally right
  right_diag = myrow + n - 1 # the column diagonal to me on the zeroith row
  for i in range
    return false if arr[i] == right_diag - i
  end
  
  true
end




board = []
b = Backtrack.new(8,1,8) {|arr| is_valid_board(arr)}
sum = 0
while b.solve!(board)
  sum += 1
  print sum, " ", board, "\n"
end


class TestBoardChecker < Test::Unit::TestCase

  def test_empty_board
    assert_equal(is_valid_board([]), true)
  end
  
  def test_one
    assert_equal(is_valid_board([1]), true)
  end
  
  def test_same_col
    assert_equal(is_valid_board([1,1]), false)
    assert_equal(is_valid_board([2,2]), false)
    assert_equal(is_valid_board([1,3,3]), false)
  end
  
  def test_left_diag
    assert_equal(is_valid_board([1,2]), false)
    assert_equal(is_valid_board([1,3]), true)
    assert_equal(is_valid_board([30,31]), false)
    assert_equal(is_valid_board([30,32]), true)
    assert_equal(is_valid_board([1,3,4]), false)
    assert_equal(is_valid_board([1,3,5]), true)
  end

  def test_right_diag
    assert_equal(is_valid_board([2,1]), false)
    assert_equal(is_valid_board([3,1]), true)
    assert_equal(is_valid_board([31,30]), false)
    assert_equal(is_valid_board([32,30]), true)
    assert_equal(is_valid_board([1,4,3]), false)
    assert_equal(true, is_valid_board([1,5,2]))
  end  
end
